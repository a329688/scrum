const AccessControl = require("accesscontrol");
const ac = new AccessControl();

exports.roles = (function() {
ac.grant("basic")
    .readAny("project")
    .readAny("card")
    .readAny("column")
    .readAny("member")

ac.grant("supervisor")
    .readAny("project")
    .createAny("project")
    .readAny("card")
    .createAny("card")
    .readAny("column")
    .createAny("column")
    .readAny("member")
    .createAny("member")

ac.grant("admin")
    .readAny("project")
    .updateAny("project")
    .createAny("project")
    .deleteAny("project")
    .readAny("card")
    .updateAny("card")
    .createAny("card")
    .deleteAny("card")
    .readAny("column")
    .updateAny("column")
    .createAny("column")
    .deleteAny("column")
    .readAny("member")
    .updateAny("member")
    .createAny("member")
    .deleteAny("member")
    
    return ac;
})();