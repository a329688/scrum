const mongoose = require('mongoose');

const schema = mongoose.Schema({
  _name: String,
  _requestDate: Date,
  _startDate: Date,
  _description: String,
  _managerId: {type: mongoose.Schema.ObjectId, ref: 'Member'},
  _ownerId: {type: mongoose.Schema.ObjectId, ref: 'Member'},
  _memberArray: [{type: mongoose.Schema.ObjectId, ref: 'Member'}],
  _columnArray:[{type: mongoose.Schema.ObjectId, ref: 'Column'}]
});

class Project {

  constructor(name, requestDate, startDate, description, managerId, ownerId, memberArray, columnArray){
    this._name = name;
    this._requestDate = requestDate;
    this._startDate = startDate;
    this._description = description;
    this._managerId = managerId;
    this._ownerId = ownerId;
    this._memberArray = memberArray;
    this._columnArray = columnArray;
  }

  get name(){
    return this._name;
  }

  set name(v){
    this._name = v;
  }
  get requestDate(){
    return this._requestDate;
  }

  set requestDate(v){
    this._requestDate = v;
  }
  get startDate(){
    return this._startDate;
  }

  set startDate(v){
    this._startDate = v;
  }

  get description(){
    return this._description;
  }

  set description(v){
    this._description = v;
  }
  get managerId(){
    return this._managerId;
  }

  set managerId(v){
    this._managerId = v;
  }
  get ownerId(){
    return this._ownerId;
  }

  set ownerId(v){
    this._ownerId = v;
  }

  get memberArray(){
    return this._memberArray;
  }

  set memberArray(v){
    this._memberArray = v;
  }

  get columnArray(){
    return this._columnArray;
  }

  set columnArray(v){
    this._columnArray = v;
  }
}

schema.loadClass(Project);
module.exports = mongoose.model('Project', schema);
