const mongoose = require('mongoose');

const schema = mongoose.Schema({
  _name:String,
  _email:String,
  _password:String,
  _birthDate:Date,
  _curp: String,
  _rfc: String,
  _address: String,
  _skillArray:[{
    _skill: String,
    _level: {
      type: String,
      enum: ["junior", "senior", "master"],
      default: "junior"
    }
  }],
  _salt: String,
  _role: String,
  _accessToken: String
});

class Member {

  constructor(name, email, password, birthDate, curp, rfc, address, skillArray, salt, role, accessToken){
    this._name = name;
    this._email = email;
    this._password = password;
    this._birthDate = birthDate;
    this._curp = curp;
    this._rfc = rfc;
    this._address = address;
    this._skillArray = skillArray;
    this._salt = salt;
    this._role = role;
    this._accessToken = accessToken
  }
  get name(){
    return this._name;
  }

  set name(v){
    this._name = v;
  }
  get email(){
    return this._email;
  }

  set email(v){
    this._email = v;
  }
  get password(){
    return this._password;
  }

  set password(v){
    this._password = v;
  }

  get birthDate(){
    return this._birthDate;
  }

  set birthDate(v){
    this._birthDate = v;
  }
  get curp(){
    return this._curp;
  }

  set curp(v){
    this._curp = v;
  }
  get rfc(){
    return this._rfc;
  }

  set rfc(v){
    this._rfc = v;
  }

  get address(){
    return this._address;
  }

  set address(v){
    this._address = v;
  }

  get skillArray(){
    return this._skillArray;
  }

  set skillArray(v){
    this._skillArray = v;
  }
  get salt(){
    return this._salt;
  }

  set salt(v){
    this._salt = v;
  }

  get role(){
    return this._role;
  }

  set role(v){
    this._role = v;
  }

  get accessToken(){
    return this._accessToken;
  }

  set accessToken(v){
    this._accessToken = v;
  }
  
}

schema.loadClass(Member);
module.exports = mongoose.model('Member', schema);
