const mongoose = require('mongoose');

const schema = mongoose.Schema({
  _name: String,
  _priority: Number,
  _size: String,
  _role: String,
  _functionality: String,
  _benefit: String,
  _context: String,
  _narrative: String,
  _criteria: String,
  _eventArray: [],
  _resultArray: []
});

class Card {

  constructor(name, priority, size, role, functionality, benefit, context, narrative, criteria, eventArray, resultArray){
    this._name = name;
    this._priority = priority;
    this._size = size;
    this._role = role;
    this._functionality = functionality;
    this._benefit = benefit;
    this._narrative = narrative;
    this._context = context;
    this._criteria = criteria;
    this._eventArray = eventArray;
    this._resultArray = resultArray;
  }

  get name(){
    return this._name;
  }

  set name(v){
    this._name = v;
  }
  get priority(){
    return this._priority;
  }

  set priority(v){
    this._priority = v;
  }
  get size(){
    return this._size;
  }

  set size(v){
    this._size = v;
  }

  get role(){
    return this._role;
  }

  set role(v){
    this._role = v;
  }
  get functionality(){
    return this._functionality;
  }

  set functionality(v){
    this._functionality = v;
  }
  get benefit(){
    return this._benefit;
  }

  set benefit(v){
    this._benefit = v;
  }

  get context(){
    return this._context;
  }

  set context(v){
    this._context = v;
  }

  get narrative(){
    return this._narrative;
  }

  set narrative(v){
    this._narrative = v;
  }

  get criteria(){
    return this._criteria;
  }

  set criteria(v){
    this._criteria = v;
  }

  get eventArray(){
    return this._eventArray;
  }

  set eventArray(v){
    this._eventArray = v;
  }

  get resultArray(){
    return this._resultArray;
  }

  set resultArray(v){
    this._resultArray = v;
  }
}

schema.loadClass(Card);
module.exports = mongoose.model('Card', schema);
