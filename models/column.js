const mongoose = require('mongoose');

const schema = mongoose.Schema({
  _name:String,
  _cardArray:[{type: mongoose.Schema.ObjectId, ref: 'Card'}]
});

class Column {

  constructor(name, cardArray){
    this._name = name;
    this._cardArray = cardArray;
  }

  get name(){
    return this._name;
  }

  set name(v){
    this._name = v;
  }

  get cardArray(){
    return this._skillArray;
  }

  set cardArray(v){
    this._skillArray = v;
  }
}

schema.loadClass(Column);
module.exports = mongoose.model('Column', schema);
