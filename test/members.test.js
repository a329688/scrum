const supertest = require('supertest');

const app = require('../app');

var key = "";
var id = "";
describe('Probar el sistema de autenticacion', ()=>{
  it('Deberia de obtener un login con usuario y contrasena correcto', (done)=>{
    supertest(app).post('/login')
    .send({
      "email":"h@g3.mx",
      "password": "hola1234!"
    })
    .expect(200)
    .end(function(err, res){
      key = res.body.obj._accessToken;
      done();
    });
  });
});

describe('Probar las rutas de los members', ()=>{
  it('Deberia crear un member', (done)=>{
    supertest(app).post('/members')
    .set('Authorization', `Bearer ${key}`)
    .send({
      "name": "WONG",
      "email":"WONG@WONG.mx",
      "password": "wongtime",
      "birthDate": "11/11/11",
      "curp": "12jkn22rjn2",
      "rfc": "jkdn23234j22j",
      "address": "La casa de WONG",
      "skillArray": [],
      "role": "admin"
    })
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        id = res.body.obj._id;
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });

  it('Deberia de enlistar members', (done)=>{
    supertest(app).get('/members/')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });

  it('Deberia de encontrar un member', (done)=>{
    supertest(app).get(`/members/${id}`)
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });

  it('Deberia reemplazar un member', (done)=>{
    supertest(app).put(`/members/${id}`)
    .set('Authorization', `Bearer ${key}`)
    .send({
      "name": "WONG2",
      "email":"WONG@WONG.mx",
      "password": "wongtime",
      "birthDate": "11/11/11",
      "curp": "12jkn22rjn2",
      "rfc": "jkdn23234j22j",
      "address": "La casa de WONG",
      "skillArray": []
    })
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });

  it('Deberia editar un member', (done)=>{
    supertest(app).patch(`/members/${id}`)
    .set('Authorization', `Bearer ${key}`)
    .send({
      "name": "WONG3",
      "email":"WONG@WONG.mx",
      "password": "wongtime",
      "birthDate": "11/11/11",
      "curp": "12jkn22rjn2",
      "rfc": "jkdn23234j22j",
      "address": "La casa de WONG",
      "skillArray": []
    })
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });

  it('Deberia borrar un member', (done)=>{
    supertest(app).delete(`/members/${id}`)
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});



describe('Probar las rutas de los members en caso de error', ()=>{
  it('Deberia dar error al crear un member', (done)=>{
    supertest(app).post('/members')
    .set('Authorization', `Bearer ${key}`)
    .send({
      "name": "WONG",
      "email":[],
      "password": "wongtime",
      "birthDate": "11/11/11",
      "curp": "12jkn22rjn2",
      "rfc": "jkdn23234j22j",
      "address": "La casa de WONG",
      "skillArray": []
    })
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        id = res.body.obj._id;
        expect(res.statusCode).toEqual(500);
        done();
      }
    });
  });

  it('Deberia dar eror al enlistar members', (done)=>{
    supertest(app).get('/members/')
    .set('Authorization', `Bearer 1`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(401);
        done();
      }
    });
  });

  it('Deberia dar error al buscar un member', (done)=>{
    supertest(app).get(`/members/${id}`)
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(500);
        done();
      }
    });
  });

  it('Deberia dar error al reemplazar un member', (done)=>{
    supertest(app).put(`/members/${id}`)
    .set('Authorization', `Bearer ${key}`)
    .send({
      "name": "WONG2",
      "email":"WONG@WONG.mx",
      "password": [],
      "birthDate": "11/11/11",
      "curp": "12jkn22rjn2",
      "rfc": "jkdn23234j22j",
      "address": "La casa de WONG",
      "skillArray": []
    })
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(500);
        done();
      }
    });
  });

  it('Deberia dar error al editar un member', (done)=>{
    supertest(app).patch(`/members/${id}`)
    .set('Authorization', `Bearer ${key}`)
    .send({
      "name": "WONG3",
      "email":"WONG@WONG.mx",
      "password": "wongtime",
      "birthDate": "11/11/11",
      "curp": "12jkn22rjn2",
      "rfc": "jkdn23234j22j",
      "address": "La casa de WONG",
      "skillArray": []
    })
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(500);
        done();
      }
    });
  });

  it('Deberia dar error al borrar un member', (done)=>{
    supertest(app).delete(`/members/${id}`)
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(500);
        done();
      }
    });
  });
});
