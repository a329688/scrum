const supertest = require('supertest');

const app = require('../app');

var key = "";
var id = "";

describe('Probar el sistema de autenticacion', ()=>{
  it('Deberia de obtener un login con usuario y contrasena correcto', (done)=>{
    supertest(app).post('/login')
    .send({
      "email":"h@g3.mx",
      "password": "hola1234!"
    })
    .expect(200)
    .end(function(err, res){
      key = res.body.obj._accessToken;
      done();
    });
  });
});

describe('Probar las rutas de los cards', ()=>{
  it('Deberia crear un card', (done)=>{
    supertest(app).post('/cards/')
    .set('Authorization', `Bearer ${key}`)
    .send({
      "name": "WONG",
      "priority": 1,
      "size": "One",
      "role": "None",
      "functionality": "None",
      "benefit": "None",
      "context": "None",
      "narrative": "None",
      "criteria": "None",
      "eventArray": [],
      "resultArray": []
    })
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        id = res.body.obj._id;
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });

  it('Deberia de enlistar cards', (done)=>{
    supertest(app).get('/cards/')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });

  it('Deberia de encontrar un card', (done)=>{
    supertest(app).get(`/cards/${id}`)
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });

  it('Deberia reemplazar un card', (done)=>{
    supertest(app).put(`/cards/${id}`)
    .set('Authorization', `Bearer ${key}`)
    .send({
      "name": "WONG2",
      "priority": 1,
      "size": "One",
      "role": "None",
      "functionality": "None",
      "benefit": "None",
      "context": "None",
      "narrative": "None",
      "criteria": "None",
      "eventArray": [],
      "resultArray": []
    })
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });

  it('Deberia editar un card', (done)=>{
    supertest(app).patch(`/cards/${id}`)
    .set('Authorization', `Bearer ${key}`)
    .send({
      "name": "WONG3",
      "priority": 1,
      "size": "One",
      "role": "None",
      "functionality": "None",
      "benefit": "None",
      "context": "None",
      "narrative": "None",
      "criteria": "None",
      "eventArray": [],
      "resultArray": []
    })
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });

  it('Deberia borrar un card', (done)=>{
    supertest(app).delete(`/cards/${id}`)
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});




describe('Probar las rutas de los cards en caso de error', ()=>{
  it('Deberia dar error al crear un card', (done)=>{
    supertest(app).post('/cards/')
    .set('Authorization', `Bearer ${key}`)
    .send({
      "name": "WONG",
      "priority": 2,
      "size": [],
      "role": "None",
      "functionality": "None",
      "benefit": "None",
      "context": "None",
      "narrative": "None",
      "criteria": "None",
      "eventArray": [],
      "resultArray": []
    })
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        id = res.body.obj._id;
        expect(res.statusCode).toEqual(500);
        done();
      }
    });
  });

  it('Deberia dar error al enlistar cards', (done)=>{
    supertest(app).get('/cards/')
    .set('Authorization', `Bearer 1`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(401);
        done();
      }
    });
  });

  it('Deberia dar eror al buscar un card', (done)=>{
    supertest(app).get(`/cards/${id}`)
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(500);
        done();
      }
    });
  });

  it('Deberia dar error al reemplazar un card', (done)=>{
    supertest(app).put(`/cards/${id}`)
    .set('Authorization', `Bearer ${key}`)
    .send({
      "name": "WONG2",
      "priority": 3,
      "size": "One",
      "role": "None",
      "functionality": "None",
      "benefit": "None",
      "context": "None",
      "narrative": "None",
      "criteria": "None",
      "eventArray": [],
      "resultArray": []
    })
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(500);
        done();
      }
    });
  });

  it('Deberia dar error al editar un card', (done)=>{
    supertest(app).patch(`/cards/${id}`)
    .set('Authorization', `Bearer ${key}`)
    .send({
      "name": "WONG3",
      "priority": 1,
      "size": "One",
      "role": "None",
      "functionality": "None",
      "benefit": "None",
      "context": "None",
      "narrative": "None",
      "criteria": "None",
      "eventArray": [],
      "resultArray": []
    })
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(500);
        done();
      }
    });
  });

  it('Deberia dar error al borrar un card', (done)=>{
    supertest(app).delete(`/cards/${id}`)
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(500);
        done();
      }
    });
  });
});
