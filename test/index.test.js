const supertest = require('supertest');

const app = require('../app');

var key = "";
describe('Probar el sistema de autenticacion', ()=>{
  it('Deberia de obtener un login con usuario y contrasena correcto', (done)=>{
    supertest(app).post('/login')
    .send({
      'email':'h@g3.mx',
      'password': 'hola1234!'
    })
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});

describe('Probar el sistema de autenticacion', ()=>{
  it('Deberia de obtener error de login', (done)=>{
    supertest(app).post('/login')
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(403);
        done();
      }
    });
  });
});
