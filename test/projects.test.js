const supertest = require('supertest');

const app = require('../app');

var key = "";
var id = "";
var otroId = "";
describe('Probar el sistema de autenticacion', ()=>{
  it('Deberia de obtener un login con usuario y contrasena correcto', (done)=>{
    supertest(app).post('/login')
    .send({
      "email":"h@g3.mx",
      "password": "hola1234!"
    })
    .expect(200)
    .end(function(err, res){
      key = res.body.obj._accessToken;
      otroId = res.body.obj._id;
      done();
    });
  });
});

describe('Probar las rutas de los projects', ()=>{
  it('Deberia crear un project', (done)=>{
    supertest(app).post('/projects')
    .set('Authorization', `Bearer ${key}`)
    .send({
      "name": "WONG",
      "requestDate": "11/11/11",
      "startDate": "11/1/11",
      "description": "None",
      "managerId": otroId,
      "ownerId": otroId,
      "memberArray": [],
      "columnArray": []
    })
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        id = res.body.obj._id;
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });

  it('Deberia de enlistar projects', (done)=>{
    supertest(app).get('/projects/')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });

  it('Deberia de encontrar un project', (done)=>{
    supertest(app).get(`/projects/${id}`)
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });

  it('Deberia reemplazar un project', (done)=>{
    supertest(app).put(`/projects/${id}`)
    .set('Authorization', `Bearer ${key}`)
    .send({
      "name": "WONG2",
      "requestDate": "11/11/11",
      "startDate": "11/1/11",
      "description": "None",
      "managerId": otroId,
      "ownerId": otroId,
      "memberArray": [],
      "columnArray": []
    })
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });

  it('Deberia editar un project', (done)=>{
    supertest(app).patch(`/projects/${id}`)
    .set('Authorization', `Bearer ${key}`)
    .send({
      "name": "WONG3",
      "requestDate": "11/11/11",
      "startDate": "11/1/11",
      "description": "None",
      "managerId": "",
      "ownerId": "",
      "memberArray": [],
      "columnArray": []
    })
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });

  it('Deberia borrar un project', (done)=>{
    supertest(app).delete(`/projects/${id}`)
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});

describe('Probar las rutas de los projects en caso de error', ()=>{
  it('Deberia dar error al crear un project', (done)=>{
    supertest(app).post('/projects')
    .set('Authorization', `Bearer ${key}`)
    .send({
      "name": "WONG",
      "requestDate": "11/11/11",
      "startDate": "11/1/11",
      "description": [],
      "managerId": "",
      "ownerId": "",
      "memberArray": [],
      "columnArray": []
    })
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        id = res.body.obj._id;
        expect(res.statusCode).toEqual(500);
        done();
      }
    });
  });

  it('Deberia dar error al enlistar projects', (done)=>{
    supertest(app).get('/projects/')
    .set('Authorization', `Bearer 1`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(401);
        done();
      }
    });
  });

  it('Deberia dar error al bucar un project', (done)=>{
    supertest(app).get(`/projects/${id}`)
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(500);
        done();
      }
    });
  });

  it('Deberia dar error al reemplazar un project', (done)=>{
    supertest(app).put(`/projects/${id}`)
    .set('Authorization', `Bearer ${key}`)
    .send({
      "name": "WONG2",
      "requestDate": "11/11/11",
      "startDate": "11/1/11",
      "description": "None",
      "managerId": "",
      "ownerId": "",
      "memberArray": [],
      "columnArray": []
    })
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(500);
        done();
      }
    });
  });

  it('Deberia dar error al editar un project', (done)=>{
    supertest(app).patch(`/projects/${id}`)
    .set('Authorization', `Bearer ${key}`)
    .send({
      "name": "WONG3",
      "requestDate": "11/11/11",
      "startDate": "11/1/11",
      "description": "None",
      "managerId": "",
      "ownerId": "",
      "memberArray": [],
      "columnArray": []
    })
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(500);
        done();
      }
    });
  });

  it('Deberia dar error al borrar un project', (done)=>{
    supertest(app).delete(`/projects/${id}`)
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(500);
        done();
      }
    });
  });
});
