const supertest = require('supertest');

const app = require('../app');

var key = "";
var id = "";
describe('Probar el sistema de autenticacion', ()=>{
  it('Deberia de obtener un login con usuario y contrasena correcto', (done)=>{
    supertest(app).post('/login')
    .send({
      "email":"h@g3.mx",
      "password": "hola1234!"
    })
    .expect(200)
    .end(function(err, res){
      key = res.body.obj._accessToken;
      done();
    });
  });
});


describe('Probar las rutas de los columns', ()=>{
  it('Deberia crear un column', (done)=>{
    supertest(app).post('/columns/')
    .set('Authorization', `Bearer ${key}`)
    .send({
      "name": "WONG",
      "cardArray": []
    })
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        id = res.body.obj._id;
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });

  it('Deberia de enlistar columns', (done)=>{
    supertest(app).get('/columns/')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });

  it('Deberia de encontrar un column', (done)=>{
    supertest(app).get(`/columns/${id}`)
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });

  it('Deberia reemplazar un column', (done)=>{
    supertest(app).put(`/columns/${id}`)
    .set('Authorization', `Bearer ${key}`)
    .send({
      "name": "WONG2",
      "cardArray": []
    })
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });

  it('Deberia editar un column', (done)=>{
    supertest(app).patch(`/columns/${id}`)
    .set('Authorization', `Bearer ${key}`)
    .send({
      "name": "WONG3",
      "cardArray": []
    })
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });

  it('Deberia borrar un column', (done)=>{
    supertest(app).delete(`/columns/${id}`)
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});




describe('Probar las rutas de los columns en caso de error', ()=>{
  it('Deberia dar error al crear un column', (done)=>{
    supertest(app).post('/columns/')
    .set('Authorization', `Bearer ${key}`)
    .send({
      "name": [],
      "cardArray": []
    })
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        id = res.body.obj._id;
        expect(res.statusCode).toEqual(500);
        done();
      }
    });
  });

  it('Deberia dar error al enlistar columns', (done)=>{
    supertest(app).get('/columns/')
    .set('Authorization', `Bearer 1`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(401);
        done();
      }
    });
  });

  it('Deberia dar error al buscar un column', (done)=>{
    supertest(app).get(`/columns/${id}`)
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(500);
        done();
      }
    });
  });

  it('Deberia dar error al reemplazar un column', (done)=>{
    supertest(app).put(`/columns/${id}`)
    .set('Authorization', `Bearer ${key}`)
    .send({
      "name": "WONG2",
      "cardArray": []
    })
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(500);
        done();
      }
    });
  });

  it('Deberia dar error al editar un column', (done)=>{
    supertest(app).patch(`/columns/${id}`)
    .set('Authorization', `Bearer ${key}`)
    .send({
      "name": "WONG3",
      "cardArray": []
    })
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(500);
        done();
      }
    });
  });

  it('Deberia dar error al borrar un column', (done)=>{
    supertest(app).delete(`/columns/${id}`)
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(500);
        done();
      }
    });
  });
});
