# Proyecto reto II: Desarrollar un manejador de proyectos

Proyecto Primer Parcial - Una aplicacion web para manejador de proyectos hecha con node usando el framework de express y conectando las aplicaciones por medio de Docker. Hasta ahora solo se tienen las rutas y los controladores pre hechos.

Proyecto Segundo Parcial - Una aplicacion web para manejador de proyectos hecha con node usando el framework de express. Se implementan todas las rutas/controlador. Se implementan componentes de seguridad (JWT y almacenamiento seguro de datos), manejo de ambientes, matriz RBAC, internacionalización y localización. Dentro existe un script de pruebas (que funciona solo si se tiene un miembro admin ya adentro de la base de datos por la naturaleza del jwt). En ambientes se controla produccion y desarrollo con diferentes bases de datos incluyento mongo en docker y mongo en atlas.

Diagrama de clases final:
![alt-text](https://lh3.googleusercontent.com/pw/AM-JKLVG1i4WybwxtmK4RzfMCpxllcE9PWEe0g5H0VaVERKN6O4EqUL1xTHBKdCsX8glTy_jpj1JhKlzxS9m7dw1zkBeFchHOAafc92AzeDK-Pp0ZvJEguCMtc_TUkGJIADxajoVBxWOHUEIYXphUzsiLAEh=w1371-h364-no?authuser=0)

Diagrama de secuencia final:
![alt-text](https://lh3.googleusercontent.com/pw/AM-JKLVLHn9dXMRcRF126RQNZ3GplOYb_aTDG-mkTIb2mIQw-M6qYkermDm_qV_Ew4eYreAVjfwAQ_i2HdzjkO910RYFg9K2WRJ5aMQvHipCGtQoKB-u9oqkGEwme14Xm8ZUigE2XJhW7s4CYF5wqhWTFta7=w639-h937-no?authuser=0)

## Getting Started

Es necesario tener docker instalado. Mas info en la parte de instalacion si es que no se tiene. Puede correrlo con npm start para produccion y npm run dev para desarrollo.

### Prerequisites

Un sistema que corra Docker y tener Docker instalado. Ademas de node si se quiere probar
Para mas info checar la documentacion de la instalacion de [Docker](https://docs.docker.com/engine/install/)
Para mas info checar la documentacion de la instalacion de [NodeJs](https://nodejs.org/en/)

### Installing

Construir la coleccion de imagenes

```
docker-compose build
```

## Running the tests

Screenshot de pruebas hechas con ``` npm run test ``` Cabe aclarar que se debe de tener un memeber inyectado manualmente a la base de datos con el rol admin para hacer el login y obtener el token de acceso. En el ss se ven los 5 archivos de prueba pasados y los 54 casos con la reglas del 50% completados

![alt-text](https://lh3.googleusercontent.com/pw/AM-JKLUP6vebRL6zcSpWovgUM2an7vdZ78iRYAt6mnUEimp2aaDSO8Nn5RDLnK4ZujS1mqLz_YQcHD-9E0_xHOJlwExO_WDaTQnMmaJ5B-ehvhQVFeLOC_U8vRO96iaUCRPdSMCnV33NT8wcJDBmju3fdZZT=w1366-h768-no?authuser=0)

Esta es la cuenta de un admin en el mongo atlas:
{
    "email": "h@g3.mx",
    "password": "hola1234!"
}

Este es el Bearer token:
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxYTgyZTg0MDdhMjg3MWVlZDVlMDY0NSIsImlhdCI6MTYzODQxMTkwOH0.ZTBOjkOaVt_aLX7UVV1l17Th7sVUBonSNSUFWWEIU04

Para correr la app es necesario construir la coleccion de contenedores
```
docker-compose up -d
```

Para detenerlo se usar
```
docker-compose down
```

Para probarlo en development
```
npm run dev
```

Para probarlo en produccion
```
npm start
```

Para correr las pruebas
```
npm run test
```

Para confirmar puede usar la ruta local con el puerto 3000.
Puede usar un navegador o postman si lo prefiere.

Las rutas locales son:
* localhost:3000/
* localhost:3000/cards
* localhost:3000/column
* localhost:3000/member
* localhost:3000/projects

Las rutas hosteadas son:
* https://proyectowebscrum.herokuapp.com/
* https://proyectowebscrum.herokuapp.com/cards
* https://proyectowebscrum.herokuapp.com/column
* https://proyectowebscrum.herokuapp.com/member
* https://proyectowebscrum.herokuapp.com/projects

Dependiendo del metodo se necesitara un id en los parametros explicitos.

## Deployment

Tenemos la aplicacion en produccion corriendo en [Heroku](https://proyectowebscrum.herokuapp.com/)

## Built With

* [Docker](https://www.docker.com/) - App Deployment
* [NodeJS](https://nodejs.org/en/) - JavaScript runtime
* [Express](https://expressjs.com/es/) - Framework de Backend
* [MongoDB](https://www.mongodb.com/) - Base de datos
* [Heroku](https://www.heroku.com/) - Hoster

## Contributing

NO HAY, NO SE ACEPTAN POR EL MOMENTO.

## Versioning

*Ver 2.0*
Para mas info ver los commits.

## Authors

* **Juan Pablo Martinez Cantu 329688** - *Modelos, 2 controladores y test* - [@a329688](https://gitlab.com/a329688)
* **Hugo Edibray Becerra Gandara 329532** - *Diagramas, 2 controladores, jwt, internacionalización y localización* - [@edibraybecerra](https://gitlab.com/edibraybecerra)
* **Ruben Dominguez Chavez 329806** - *1 controlador, RBAC, rutas y merges* - [@a329806](https://gitlab.com/a329806)

## License

ICS License

## Acknowledgments

* Un saludo a Hector "el patito" Aguirre y a Carlos Tomas "Tommy" Garcia
* Al wong que ya no esta con nosotros unu (no esta muerto)
* y pos ya, supongo que al profe :)
