var express = require('express');
var router = express.Router();

const controller = require('../controllers/projects')
const controllerAuth = require('../controllers/members');

/* GET users listing. */
router.get('/', controllerAuth.grantAccess('readAny', 'project'), controller.list);

router.get('/:id', controllerAuth.grantAccess('readAny', 'project'), controller.find);

router.put('/:id', controllerAuth.grantAccess('updateAny', 'project'), controller.replace)

router.patch('/:id', controllerAuth.grantAccess('updateAny', 'project'), controller.update);

router.delete('/:id', controllerAuth.grantAccess('deleteAny', 'project'), controller.erase);

router.post('/', controllerAuth.grantAccess('createAny', 'project'), controller.add);

module.exports = router;
