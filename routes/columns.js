var express = require('express');
var router = express.Router();

const controller = require('../controllers/columns')
const controllerAuth = require('../controllers/members');

router.get('/', controllerAuth.grantAccess('readAny', 'column'), controller.list);

router.get('/:id', controllerAuth.grantAccess('readAny', 'column'), controller.find);

router.put('/:id', controllerAuth.grantAccess('updateAny', 'column'), controller.replace);

router.patch('/:id', controllerAuth.grantAccess('updateAny', 'column'), controller.update);

router.delete('/:id', controllerAuth.grantAccess('deleteAny', 'column'), controller.erase);

router.post('/', controllerAuth.grantAccess('createAny', 'column'), controller.add);

module.exports = router;
