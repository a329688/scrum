var express = require('express');
var router = express.Router();

const controller = require('../controllers/cards');
const controllerAuth = require('../controllers/members');

/* GET users listing. */
router.get('/', controllerAuth.grantAccess('readAny', 'card'), controller.list);

router.get('/:id', controllerAuth.grantAccess('readAny', 'card'), controller.find);

router.put('/:id', controllerAuth.grantAccess('updateAny', 'card'), controller.replace);

router.patch('/:id', controllerAuth.grantAccess('updateAny', 'card'), controller.update);

router.delete('/:id', controllerAuth.grantAccess('deleteAny', 'card'), controller.erase);

router.post('/', controllerAuth.grantAccess('createAny', 'card'), controller.add);

module.exports = router;
