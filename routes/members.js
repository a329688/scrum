const express = require('express');
const router = express.Router();
const controller = require('../controllers/members');

/* GET users listing. */
router.get('/', controller.grantAccess('readAny', 'member'), controller.list);

router.get('/:id', controller.grantAccess('readAny', 'member'), controller.find);

router.put('/:id', controller.grantAccess('updateAny', 'member'), controller.replace)

router.patch('/:id', controller.grantAccess('updateAny', 'member'), controller.update);

router.delete('/:id', controller.grantAccess('deleteAny', 'member'), controller.erase);

router.post('/', controller.grantAccess('createAny', 'member'), controller.add);

module.exports = router;
