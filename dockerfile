FROM node   
MAINTAINER Hugo Becerra
WORKDIR /app
COPY . . 
RUN npm install
EXPOSE 3000
CMD npm start