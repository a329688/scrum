const express = require('express');
const bcrypt = require('bcrypt');
const async = require('async');
const jwt = require('jsonwebtoken');
const config = require('config');
var log4js = require("log4js");
var logger = log4js.getLogger();

const Member = require ('../models/member');

const jwtKey = config.get("secret.key");

function home(req, res, next) {
  res.render('index', { title: 'Buenas' });
}


function login(req, res, next) {
  let email = req.body.email;
  let password = req.body.password;

  async.parallel({
    member: callback => Member.findOne({_email:email})
    .select('_password _salt')
    .exec(callback)
  }, (err, result) => {
    if(result.member) {
      bcrypt.hash(password, result.member.salt, async (err, hash) => {
        if(hash === result.member.password) {

          const accessToken = jwt.sign({id: result.member._id}, jwtKey);
          await Member.findByIdAndUpdate(result.member._id, { accessToken }).then(obj => {
            logger.level = "info";
            logger.info(res.__('ok.login'));
            res.status(200).json({
              message: res.__('ok.login'),
              "obj": obj,
              accessToken: accessToken
            });
          }).catch(ex => {
            logger.level = "error";
            logger.info(res.__('bad.login'));
            res.status(500).json({
              message: res.__('bad.login'),
              "obj": ex
            })
          });

          
        } else {
            logger.level="error";
            logger.error(res.__('bad.login'));
            res.status(403).json({
            message:res.__('bad.login'),
            "obj":null
          });
        }
      });
    } else {
      logger.level="error";
      logger.error(res.__('bad.login'));
      res.status(403).json({
        message:res.__('bad.login'),
        "obj":null
      });
    }
  });
}


module.exports = {
  home, login
}
