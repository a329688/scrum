const express = require('express');
const Card = require('../models/card');
const log4js = require('log4js');
const i18n = require('i18n');

logger = log4js.getLogger();

function list(req, res, next) {
  Card.find().then(objs => {
    logger.level = "info";
    logger.info(res.__('list.cards'));
    res.status(200).json({
      message: res.__('list.cards'),
      obj:objs
    })
  }).catch(ex => {
    logger.level = "error";
    logger.error(res.__('badList.cards'));
    res.status(500).json({
      message: res.__('badList.cards'),
      obj:ex
    })
  });
}


function find(req, res, next) {
  const id = req.params.id;
  Card.findOne({"_id":id}).then(objs => {
    logger.level = "info";
    logger.info(`${res.__('find.cards')} ${id}`);
    res.status(200).json({
      message: `${res.__('find.cards')} ${id}`,
      obj:objs
    })
  }).catch(ex => {
    logger.level = "error";
    logger.error(`${res.__('badFind.cards')} ${id}`);
    res.status(500).json({
      message: `${res.__('badFind.cards')} ${id}`,
      obj:ex
    })
  });
}



function add(req, res, next) {
  const name = req.body.name;
  const priority = req.body.priority;
  const size  = req.body.size;
  const role = req.body.role;
  const functionality = req.body.functionality;
  const benefit = req.body.benefit;
  const context = req.body.context;
  const narrative = req.body.narrative;
  const criteria = req.body.criteria;
  const eventArray = req.body.eventArray;
  const resultArray = req.body.resultArray;

  let card = new Card({
    _name:name,
    _priority:priority,
    _size:size,
    _role:role,
    _functionality:functionality,
    _benefit:benefit,
    _context:context,
    _narrative:narrative,
    _criteria:criteria,
    _eventArray:eventArray,
    _resultArray:resultArray
  });

  card.save().then(objs => {
    logger.level = "info";
    logger.info(res.__('add.cards'));
    res.status(200).json({
      message: res.__('add.cards'),
      obj:objs
    })
  }).catch(ex => {
    logger.level = "error";
    logger.error(res.__('badAdd.cards'));
    res.status(500).json({
      message: res.__('badAdd.cards'),
      obj:ex
    })
  });
}

function replace(req, res, next) {
  const id = req.params.id;
  const name = req.body.name ? req.body.name : "";
  const priority = req.body.priority ? req.body.priority : 0;
  const size  = req.body.size ? req.body.size : "";
  const role = req.body.role ? req.body.role : "";
  const functionality = req.body.functionality ? req.body.functionality : "";
  const benefit = req.body.benefit ? req.body.benefit : "";
  const context = req.body.context ? req.body.context : "";
  const narrative = req.body.narrative ? req.body.narrative : "";
  const criteria = req.body.criteria ? req.body.criteria : "";
  const eventArray = req.body.eventArray ? req.body.eventArray : [];
  const resultArray = req.body.resultArray ? req.body.resultArray : [];

  let card = new Object({
    _name:name,
    _priority:priority,
    _size:size,
    _role:role,
    _functionality:functionality,
    _benefit:benefit,
    _context:context,
    _narrative:narrative,
    _criteria:criteria,
    _eventArray:eventArray,
    _resultArray:resultArray
  });

  Card.findOneAndUpdate({"_id":id}, card).then(objs => {
    logger.level = "info";
    logger.info(`${res.__('replace.cards')} ${id}`);
    res.status(200).json({
      message: `${res.__('replace.cards')} ${id}`,
      obj:objs
    })
  }).catch(ex => {
    logger.level = "error";
    logger.error(`${res.__('badReplace.cards')} ${id}`);
    res.status(500).json({
      message: `${res.__('badReplace.cards')} ${id}`,
      obj:ex
    })
  });

}

function update(req, res, next) {
  const id = req.params.id;
  const name = req.body.name;
  const priority = req.body.priority;
  const size  = req.body.size;
  const role = req.body.role;
  const functionality = req.body.functionality;
  const benefit = req.body.benefit;
  const context = req.body.context;
  const narrative = req.body.narrative;
  const criteria = req.body.criteria;
  const eventArray = req.body.eventArray;
  const resultArray = req.body.resultArray;

  let card = new Object();

  if(name){
    card._name = name;
  }

  if(priority){
    card._priority = priority;
  }

  if(size){
    card._size = size;
  }

  if(role){
    card._role = role;
  }

  if(functionality){
    card._functionality = functionality;
  }
  if(benefit){
    card._benefit = benefit;
  }
  if(context){
    card._context = context;
  }
  if(narrative){
    card._narrative = narrative;
  }
  if(criteria){
    card._criteria = criteria;
  }
  if(eventArray){
    card._eventArray = eventArray;
  }
  if(resultArray){
    card._resultArray = resultArray;
  }

  Card.findOneAndUpdate({"_id":id}, card).then(objs => {
    logger.level = "info";
    logger.info(`${res.__('update.cards')} ${id}`);
    res.status(200).json({
      message: `${res.__('update.cards')} ${id}`,
      obj:objs
    })
  }).catch(ex => {
    logger.level = "error";
    logger.error(`${res.__('badUpdate.cards')} ${id}`);
    res.status(500).json({
      message: `${res.__('badUpdate.cards')} ${id}`,
      obj:ex
    })
  });

}

function erase(req, res, next) {
  const id = req.params.id;
  Card.remove({"_id":id}).then(objs => {
    logger.level = "info";
    logger.info(`${res.__('erase.cards')} ${id}`);
    res.status(200).json({
      message: `${res.__('erase.cards')} ${id}`,
      obj:objs
    })
  }).catch(ex => {
    logger.level = "error";
    logger.error(`${res.__('badErase.cards')} ${id}`);
    res.status(500).json({
      message: `${res.__('badErase.cards')} ${id}`,
      obj:ex
    })
  });
}

module.exports = {
  list, find, replace, update, erase, add
}
