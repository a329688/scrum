const express = require('express');
const Project = require('../models/project');
const log4js = require('log4js');
const i18n = require('i18n');

var logger = log4js.getLogger();

// RESTFULL => GET, POST, PUT, PATCH, DELETE = Modelo
// representacion de una estructura de datos
function list(req, res, next) {
  Project.find().populate("_managerId").populate("_ownerId").populate("_memberArray").populate("_columnArray").then(obj => {
        logger.level = "info";
        logger.info(res.__('list.projects'));
        res.status(200).json({
          message: res.__('list.projects'),
          objs: obj
    })}).catch(ex => {
        logger.level = "error";
        logger.error(res.__('badList.projects'));
        res.status(500).json({
          message: res.__('badList.projects'),
          objs: ex
    })});
}

function find(req, res, next) {
  const id = req.params.id;
  Project.findOne({"_id":id}).populate("_managerId").populate("_ownerId").populate("_memberArray").populate("_columnArray").then(objs => {
      logger.level = "info";
      logger.info(`${res.__('find.projects')} ${id}`);
      res.status(200).json({
        message: `${res.__('find.projects')} ${id}`,
        obj: objs
    })
  }).catch(ex => {
      logger.level = "error";
      logger.error(`${res.__('badFind.projects')} ${id}`);
      res.status(500).json({
        message: `${res.__('badFind.projects')} ${id}`,
        obj: ex
    })
  });
}

function add(req, res, next){
  const {
    name,
    requestDate,
    startDate,
    description,
    managerId,
    ownerId,
    memberArray,
    columnArray
  } = req.body;

  const project = new Project({
    _name: name,
    _requestDate: requestDate,
    _startDate: startDate,
    _description: description,
    _managerId: managerId,
    _ownerId: ownerId,
    _memberArray: memberArray,
    _columnArray: columnArray
  });

  project.save().then(objs => {
      logger.level = "info";
      logger.info(`${res.__('add.projects')}`);
      res.status(200).json({
        message: `${res.__('add.projects')}`,
        obj: objs
    })
  }).catch(ex => {
      logger.level = "error";
      logger.error(res.__('badAdd.projects'));
      res.status(500).json({
        message: res.__('badAdd.projects'),
        obj: ex
    })
  });

}

function replace(req, res, next) {
    // name,
    // requestDate,
    // startDate,
    // description,
    // managerId,
    // ownerId,
    // memberArray,
    // columnArray
  const id = req.params.id;
  const name = req.body.name ? req.body.name : "";
  const requestDate = req.body.requestDate ? req.body.requestDate : "";
  const startDate = req.body.startDate ? req.body.startDate : "";
  const description = req.body.description ? req.body.description : "";
  const managerId = req.body.managerId ? req.body.managerId : "";
  const ownerId = req.body.ownerId ? req.body.ownerId : "";
  const memberArray = req.body.memberArray ? req.body.memberArray : [];
  const columnArray = req.body.columnArray ? req.body.columnArray : [];

  let project = new Object({
    _name: name,
    _requestDate: requestDate,
    _startDate: startDate,
    _description: description,
    _managerId: managerId,
    _ownerId: ownerId,
    _memberArray: memberArray,
    _columnArray: columnArray
  });

  Project.findOneAndUpdate({"_id":id}, project).then(objs => {
      logger.level = "info";
      logger.info(`${res.__('replace.projects')} ${id}`);
      res.status(200).json({
        message: `${res.__('replace.projects')} ${id}`,
        obj: objs
    })
  }).catch(ex => {
      logger.level = "error";
      logger.error(`${res.__('badReplace.projects')} ${id}`);
      res.status(500).json({
        message: `${res.__('badReplace.projects')} ${id}`,
        obj: ex
    })
  });
}

async function update(req, res, next) {
  const id = req.params.id;
  const {
    name,
    requestDate,
    startDate,
    description,
    managerId,
    ownerId,
    memberArray,
    columnArray
  } = req.body;

  let project = new Object();

  if(name) {
    project._name = name;
  }
  if(requestDate) {
    project._requestDate = requestDate;
  }
  if(startDate) {
    project._startDate = startDate;
  }
  if(description) {
    project._description = description;
  }
  if(managerId) {
    project._managerId = managerId;
  }
  if(ownerId) {
    project._ownerId = ownerId;
  }
  if(memberArray) {
    project._memberArray = memberArray;
  }
  if(columnArray) {
    project._columnArray = columnArray;
  }

  Project.findOneAndUpdate({"_id":id}, project).then(objs => {
      logger.level = "info";
      logger.info(`${res.__('update.projects')} ${id}`);
      res.status(200).json({
        message: `${res.__('update.projects')} ${id}`,
        obj: objs
    })
  }).catch(ex => {
      logger.level = "error";
      logger.error(`${res.__('badUpdate.projects')} ${id}`);
      res.status(500).json({
        message: `${res.__('badUpdate.projects')} ${id}`,
        obj: ex
    })
  });
}

function erase(req, res, next) {
  const id = req.params.id;
  Project.remove({"_id":id}).then(objs => {
      logger.level = "info";
      logger.info(`${res.__('erase.projects')} ${id}`);
      res.status(200).json({
        message: `${res.__('erase.projects')} ${id}`,
        obj: objs
    })
  }).catch(ex => {
      logger.level = "error";
      logger.error(`${res.__('badErase.projects')} ${id}`);
      res.status(500).json({
        message: `${res.__('badErase.projects')} ${id}`,
        obj: ex
    })
  });
}

module.exports = {
  list, find, add, replace, update, erase
}
