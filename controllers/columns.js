const express = require('express');
const Column = require('../models/column');
const log4js = require('log4js');
const i18n = require('i18n');

logger = log4js.getLogger();

function list(req, res, next) {
  Column.find().populate("_cardArray").then(objs => {
    logger.level = "info";
    logger.info(res.__('list.column'));
    res.status(200).json({
      message: res.__('list.column'),
      obj:objs
    })
  }).catch(ex => {
    logger.level = "error";
    logger.error(res.__('badList.column'));
    res.status(500).json({
      message: res.__('badList.column'),
      obj:ex
    })
  });
}


function find(req, res, next) {
  const id = req.params.id;
  Column.findOne({"_id":id}).populate("_cardArray").then(objs => {
    logger.level = "info";
    logger.info(`${res.__('find.column')} ${id}`);
    res.status(200).json({
      message: `${res.__('find.column')} ${id}`,
      obj:objs
    })
  }).catch(ex => {
    logger.level = "error";
    logger.error(`${res.__('badFind.column')} ${id}`);
    res.status(500).json({
      message: `${res.__('badFind.column')} ${id}`,
      obj:ex
    })
  });
}



function add(req, res, next) {
  const name = req.body.name;
  const cardArray  = req.body.cardArray;

  let column = new Column({
    _name:name,
    _cardArray:cardArray,
  });

  column.save().then(objs => {
    logger.level = "info";
    logger.info(res.__('add.column'));
    res.status(200).json({
      message: res.__('add.column'),
      obj:objs
    })
  }).catch(ex => {
    logger.level = "error";
    logger.error(res.__('badAdd.column'));
    res.status(500).json({
      message: res.__('badAdd.column'),
      obj:ex
    })
  });
}

function replace(req, res, next) {
  const id = req.params.id;
  const name = req.body.name ? req.body.name : "";
  const cardArray  = req.body.cardArray ? req.body.cardArray : "";

  let column = new Object({
    _name:name,
    _cardArray:cardArray
  });

  Column.findOneAndUpdate({"_id":id}, column).then(objs => {
    logger.level = "info";
    logger.info(`${res.__('replace.column')} ${id}`);
    res.status(200).json({
      message: `${res.__('replace.column')} ${id}`,
      obj:objs
    })
  }).catch(ex => {
    logger.level = "error";
    logger.error(`${res.__('badReplace.column')} ${id}`);
    res.status(500).json({
      message: `${res.__('badReplace.column')} ${id}`,
      obj:ex
    })
  });

}

function update(req, res, next) {
  const id = req.params.id;
  const name = req.body.name;
  const cardArray  = req.body.cardArray;

  let column = new Object();

  if(name){
    column._name = name;
  }
  if(cardArray){
    column._cardArray = cardArray;
  }

  Column.findOneAndUpdate({"_id":id}, column).then(objs => {
    logger.level = "info";
    logger.info(`${res.__('update.column')} ${id}`);
    res.status(200).json({
      message: `${res.__('update.column')} ${id}`,
      obj:objs
    })
  }).catch(ex => {
    logger.level = "error";
    logger.error(`${res.__('badUpdate.column')} ${id}`);
    res.status(500).json({
      message: `${res.__('badUpdate.column')} ${id}`,
      obj:ex
    })
  });

}

function erase(req, res, next) {
  const id = req.params.id;
  Column.remove({"_id":id}).then(objs => {
    logger.level = "info";
    logger.info(`${res.__('erase.column')} ${id}`);
    res.status(200).json({
      message: `${res.__('erase.column')} ${id}`,
      obj:objs
    })
  }).catch(ex => {
    logger.level = "error";
    logger.error(`${res.__('badErase.column')} ${id}`);
    res.status(500).json({
      message: `${res.__('badErase.column')} ${id}`,
      obj:ex
    })
  });
}

module.exports = {
  list, find, replace, update, erase, add
}
