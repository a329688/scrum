const express = require('express');
const Member = require('../models/member');
var log4js = require("log4js");
var logger = log4js.getLogger();
const async = require('async');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const config = require('config');
const { roles } = require('../roles');

const jwtKey = config.get("secret.key");

// RESTFULL => GET, POST, PUT, PATCH, DELETE = Modelo
// representacion de una estructura de datos
function list(req, res, next) {
  Member.find().then(objs =>{
    logger.level = "info";
    logger.info(res.__('list.member'));
    res.status(200).json({
    message: res.__('list.member'),
    obj: objs
  })})
  .catch(
    ex => {
    logger.level="error";
    logger.error(res.__('badList.member'));
    res.status(500).json({
    message: res.__('badList.member'),
    obj: ex
  })}
  );
}

function find(req, res, next) {
  const id = req.params.id;
  Member.findOne({"_id":id}).then(obj =>
    {
      logger.level = "info";
      logger.info(`${res.__('find.member')} ${id}`);
      res.status(200).json({
      message: `${res.__('find.member')} ${id}`,
      obj: obj
    })})
    .catch(
      ex => {
      logger.level="error";
      logger.error(`${res.__('badFind.member')} ${id}`);
      res.status(500).json({
      message: `${res.__('badFind.member')} ${id}`,
      obj: ex
    })}
    );
  }

async function add(req, res, next){
  const name = req.body.name;
  const email = req.body.email;
  const password = req.body.password;
  const birthDate = req.body.birthDate;
  const curp = req.body.curp;
  const rfc = req.body.rfc;
  const address = req.body.address;
  const skillArray = req.body.skillArray;
  const role = req.body.role;

  async.parallel({
    salt:(callback) => {
      bcrypt.genSalt(10, callback);
    }
  }, (err, result) => {
    bcrypt.hash(password, result.salt, async (err, hash) =>{
      let member = new Member({
        name: name,
        email: email,
        password: hash,
        birthDate: birthDate,
        curp: curp,
        rfc: rfc,
        address: address,
        skillArray: skillArray,
        salt: result.salt,
        role: role || 'basic'
      });

      const accessToken = jwt.sign({id: member._id}, jwtKey);
      member.accessToken = accessToken;

      await member.save().then(obj => {
        logger.level = "info";
        logger.info(res.__('add.member'));
        res.status(200).json({
          message: res.__('add.member'),
          obj: obj,
          accessToken
        });
      }).catch(ex =>
        {
        logger.level = "error";
        logger.info(res.__('badAdd.member'));
        res.status(500).json({
          message: res.__('badAdd.member'),
          obj: ex
      })});

    });

  });
}

async function replace (req, res, next) {
  const id = req.params.id;
  const name = req.body.name ? req.body.name : "";
  const email = req.body.email ? req.body.email: "";
  const password = req.body.password ? req.body.password: "";
  const birthDate = req.body.birthDate ? req.body.birthDate: "";
  const curp = req.body.curp ? req.body.curp: "";
  const rfc = req.body.rfc ? req.body.rfc: "";
  const address = req.body.address ? req.body.address: "";
  const skillArray = req.body.skillArray ? req.body.skillArray: [];

  async.parallel({
    salt:(callback) => {
      bcrypt.genSalt(10, callback);
    }
  }, (err, result) => {
    bcrypt.hash(password, result.salt, async (err, hash) =>{
      let member = new Object({
        _name: name,
        _email: email,
        _password: hash,
        _birthDate: birthDate,
        _curp: curp,
        _rfc: rfc,
        _address: address,
        _skillArray: skillArray,
        _salt: result.salt,
      });
      await Member.findOneAndUpdate({"_id":id}, member).then(obj =>{
        logger.level = "info";
        logger.info(`${res.__('replace.member')} ${id}`);
        res.status(200).json({
        message: `${res.__('replace.member')} ${id}`,
        obj: obj
      })})
      .catch(ex => {
        logger.level = "error";
        logger.info(`${res.__('badReplace.member')} ${id}`);
        res.status(500).json({
        message: `${res.__('badReplace.member')} ${id}`,
        obj: ex
      })});

    });

  });
}

async function update(req, res, next) {
  const id = req.params.id;
  const name = req.body.name;
  const email = req.body.email;
  const password = req.body.password;
  const birthDate = req.body.birthDate;
  const curp = req.body.curp;
  const rfc= req.body.rfc;
  const address = req.body.address;
  const skillArray = req.body.skillArray;

  async.parallel({
    salt:(callback) => {
      bcrypt.genSalt(10, callback);
    }
  }, (err, result) => {
    bcrypt.hash(password, result.salt, async (err, hash) =>{
      let member = new Object();

      if(name) {
        member._name = name;
      }
      if(email) {
        member._email = email;
      }
      if(password) {
        member._password = password;
      }
      if(birthDate) {
        member._birthDate = birthDate;
      }
      if(curp) {
        member._curp = curp;
      }
      if(rfc) {
        member._rfc = rfc;
      }
      if(address) {
        member._address = address;
      }
      if(skillArray){
        member._skillArray = skillArray;
      }
      await Member.findOneAndUpdate({"_id":id}, member).then(obj =>
        {
          logger.level = "info";
          logger.info(`${res.__('update.member')} ${id}`);
          res.status(200).json({
          message: `${res.__('update.member')} ${id}`,
          obj: obj
        })})
        .catch(
          ex => {
          logger.level="error";
          logger.error(`${res.__('badUpdate.member')} ${id}`);
          res.status(500).json({
            message: `${res.__('badUpdate.member')} ${id}`,
            obj: ex
          })
        });

    });

  });

}

function erase(req, res, next) {
  const id = req.params.id;
  Member.remove({"_id":id}).then(obj =>{
    logger.level = "info";
    logger.info(`${res.__('erase.member')} ${id}`);
    res.status(200).json({
    message: `${res.__('erase.member')} ${id}`,
    obj: obj
  })})
  .catch(
    ex => {
    logger.level="error";
    logger.error(`${res.__('badErase.member')} ${id}`);
    res.status(500).json({
    message: `${res.__('badErase.member')} ${id}`,
    obj: ex
  })}
  );
}

function grantAccess(action, resource) {
  return async (req, res, next) => {
    try {
      let tkn = req.header('Authorization')
      let token = tkn.split(' ');
      let decoded = jwt.decode(token[1], {json: true});
      var role = null;

      await Member.findOne({"_id":decoded.id}).then(obj => {
        role = obj.role;
      })
      .catch(
        ex => {
          logger.level="error";
          logger.error(res.__('bad.access'));
        }
      );

      const permission = roles.can(role)[action](resource);
      if (!permission.granted) {
        logger.level="error";
        logger.error(res.__('bad.access'));
        return res.status(401).json({
          error: res.__('bad.access')
        });
      }
      next()
    } catch (ex) {
      next()
    }  
  }
}

module.exports= {
  list, find, replace, update, erase, add, grantAccess
}
