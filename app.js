var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
// var logger = require('morgan');
var config = require('config');
var mongoose = require('mongoose');
var log4js = require('log4js');
var i18n = require('i18n');
const jwt = require('jsonwebtoken');
const expressJwt = require('express-jwt');
const bodyParser = require('body-parser');

var indexRouter = require('./routes/index');
var cardsRouter = require('./routes/cards');
var projectsRouter = require('./routes/projects');
var membersRouter = require('./routes/members');
var columnsRouter = require('./routes/columns');

var app = express();

var logger = log4js.getLogger();

const jwtKey = config.get("secret.key");

// mongodb://localhost:27017/scrum
// "mongodb://<dbuser>?:<dbPassword>?@<direction>:<port>/<dbName>"
const uri = config.get("dbChain");
mongoose.connect(uri);
const db = mongoose.connection;

i18n.configure({
  locales:['es', 'en'],
  cookie: 'language',
  directory: `${__dirname}/locales`
});

app.use(i18n.init);
app.use(expressJwt({secret:jwtKey, algorithms:['HS256']})
   .unless({path:["/login"]}));
app.use(cookieParser());
app.use(express.json());

db.on('error', () => {
  logger.level = 'error';
  logger.error("No se ha podido establecer conexion a la base de datos");
});

db.on('open', () => {
  app = express();
  logger.level = 'info';
  logger.info("Conexion correcta");
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/cards', cardsRouter);
app.use('/projects', projectsRouter);
app.use('/columns', columnsRouter);
app.use('/members', membersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
